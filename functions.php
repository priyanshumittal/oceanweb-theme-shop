<?php
/**
 * Marketify
 *
 * Do not modify this file. Place all modifications in a child theme.
 */

if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

class Marketify {

    private static $instance;

    public $helpers;

    public $customizer;

    public $activation;

    public $integrations;
    public $widgets;

    public $template;

    public $page_settings;
	public $widgetized_pages;

    public static function instance() {
        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Marketify ) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct() {
        $this->base();
        $this->setup();
    }

    // Integration getter helper
    public function get( $integration ) {
        return $this->integrations->get( $integration );
    }

    private function base() {
        $this->files = array(
            'customizer/class-customizer.php',

            'activation/class-activation.php',

            'setup/class-setup.php',

            'class-helpers.php',

            'integrations/class-integration.php',
            'integrations/class-integrations.php',

            'widgets/class-widgets.php',
            'widgets/class-widget.php',

            'template/class-template.php',

            'pages/class-page-settings.php',
            'pages/class-widgetized-page.php',

            'deprecated.php'
        );

        foreach ( $this->files as $file ) {
            require_once( get_template_directory() . '/inc/' . $file );
        }
    }

    private function setup() {
        $this->helpers = new Marketify_Helpers();

        $this->customizer = new Marketify_Customizer();

        $this->activation = new Marketify_Activation();

        $this->integrations = new Marketify_Integrations();
        $this->widgets = new Marketify_Widgets();

        $this->template = new Marketify_Template();

		$this->widgetized_pages = new Marketify_Widgetized_Pages();

       // $this->page_settings = new Marketify_Page_Settings();

        add_action( 'after_setup_theme', array( $this, 'setup_theme' ) );
    }

    public function setup_theme() {
        $locale = apply_filters( 'plugin_locale', get_locale(), 'marketify' );
        load_textdomain( 'marketify', WP_LANG_DIR . "/marketify-$locale.mo" );
        load_theme_textdomain( 'marketify', get_template_directory() . '/languages' );

        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' );

        add_editor_style( 'css/editor-style.css' );

        add_theme_support( 'custom-background', apply_filters( 'marketify_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        ) ) );

        if (apply_filters( 'marketify_hard_crop_images', true ) ) { 
            add_image_size( 'medium', get_option( 'medium_size_w' ), get_option( 'medium_size_h' ), true );
            add_image_size( 'large', get_option( 'large_size_w' ), get_option( 'large_size_h' ), true );
        }
    }

}

function marketify() {
    return Marketify::instance();
}
//update_option('download-label-plural','webthemes');
marketify();

//if (!defined('EDD_SLUG'))
//define('EDD_SLUG', 'webthemes');

/* Remove Query String From Resources */
function ocean_remove_script_version( $src )
{
	if (is_admin() || is_user_logged_in())
		return $src;

	if (strpos($src,"font") > 0)
		return $src;

	$parts = explode( '?', $src );
		return $parts[0];
}

add_filter( 'script_loader_src', 'ocean_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'ocean_remove_script_version', 15, 1 );

function custom_dequeue() {
    wp_dequeue_style('marketify-fonts');
    wp_deregister_style('marketify-fonts');
}
/*add_action( 'wp_enqueue_scripts', 'custom_dequeue', 9999 );*/
add_filter( 'get_the_archive_title', function ( $title ) {
    if( is_category() ) {
        $title = 'Our Blog';
    }
    return $title;
});

	
/**
 * Disable admin bar on the frontend of your website
 * for subscribers.
 */
function themeblvd_disable_admin_bar() {
    if ( ! current_user_can('edit_posts') ) {
        add_filter('show_admin_bar', '__return_false');
    }
}
add_action( 'after_setup_theme', 'themeblvd_disable_admin_bar' );
 /**
 * Redirect back to homepage and not allow access to
 * WP admin for Subscribers.
 */
function themeblvd_redirect_admin(){
    if ( ! defined('DOING_AJAX') && ! current_user_can('edit_posts') ) {
        wp_redirect( site_url() );
        exit;      
    }
}
add_action( 'admin_init', 'themeblvd_redirect_admin' );

add_filter('wp_nav_menu_items', 'add_login_logout_link', 10, 2); 
function add_login_logout_link($items, $args) {  
    if ( is_user_logged_in() ) {
       ob_start(); 
        wp_loginout('index.php'); 
        $loginoutlink = ob_get_contents();
         ob_end_clean();
       $items .= '<li>'. $loginoutlink .'</li>';
	}   
  return $items; 
}

function my_child_theme_edd_format_amount_decimals( $decimals, $amount ) {
	if( floor( $amount ) == $amount )
		$decimals = 0;
	return $decimals;
}
add_filter( 'edd_format_amount_decimals', 'my_child_theme_edd_format_amount_decimals', 10, 2 );

add_action('init','ocean_custom_login');
function ocean_custom_login(){
 global $pagenow;
 if( 'wp-login.php' == $pagenow && !is_user_logged_in()) {
  wp_redirect('https://oceanwebthemes.com/login/');
  exit();
 }
}