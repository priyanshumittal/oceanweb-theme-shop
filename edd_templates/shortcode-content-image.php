<?php
/**
 *
 */

global $post;
?>

<?php do_action( 'marketify_download_content_image_before' ); ?>

<div class="content-grid-download__entry-image">
    <div class="content-grid-download__overlay">
        <?php do_action( 'marketify_download_content_image_overlay_before' ); ?>

        <div class="content-grid-download__actions">
            <?php do_action( 'marketify_download_content_actions_before' ); ?>

            <a href="<?php the_permalink(); ?>" rel="bookmark" class="button button--color-white"><?php _e( 'Details', 'marketify' ); ?></a>

            <strong class="item-price"><span><?php printf( __( 'Item Price: %s', 'marketify' ), edd_price( get_the_ID(), false ) ); ?></span></strong>

            <?php do_action( 'marketify_download_content_actions_after' ); ?>
        </div>
    </div>

<?php if (0) { $image_arr = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
 $medium_array = image_downsize( get_post_thumbnail_id($post->ID), 'medium' );
    $medium_path = $medium_array[0];
        $image_url = $image_arr[0]; 
$image_url = str_replace('.jpg','-740x600.jpg',$image_url);
$image_url = str_replace('.png','-740x600.png',$image_url);

echo '<img width="740" height="600" src="' . $image_url. '" class="attachment-medium size-medium wp-post-image" />';
}?>
    <?php the_post_thumbnail( 'medium' );  ?>
</div>

<?php locate_template( array( 'modal-download-purchase.php' ), true, false ); ?>

<?php do_action( 'marketify_download_content_image_after' ); ?>
