<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Marketify
 */

get_header(); ?>

    <?php do_action( 'marketify_entry_before' ); ?>

    <div class="container">
        <div id="content" class="site-content row">
<?php $edd_prc3 = edd_price($post->ID,false);?>
            <div role="main" class="content-area <?php echo (strpos($edd_prc3,'39') !== false || !is_active_sidebar( 'sidebar-download-single' ) ) ? 'col-xs-12' : 'col-xs-12 col-md-8'; ?>">

                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'content-single', 'download' ); ?>
                <?php endwhile; rewind_posts(); ?>

            </div>

            <?php if (1 &&  strpos($edd_prc3,'39') == false) { get_sidebar( 'single-download' );} ?>

        </div><!-- #content -->
<section>
<?php $edd_prc2 = edd_price($post->ID,false); if (strpos($edd_prc2,'39')!==false) woothemes_features(array( 'limit' => 10, 'size' => 100 ,'category' => 'home-page-features') ); ?>
</section>
        <?php if (strpos($edd_prc3,'39') == false)  {comments_template();} ?>

        <?php do_action( 'marketify_single_download_after' ); ?>
    </div>
<?php $edd_prc = edd_price($post->ID,false); if (1 && strpos($edd_prc,'39')!==false) { ?>
<aside class="pt_container"><div class="container">
<section>

 <div class="pricing-table group">
<!-- PERSONAL -->
            <div class="block personal fl">
                <h2 class="title">Single Theme</h2>
                <!-- CONTENT -->
                <div class="content">
                    <p class="price">
                        <sup>$</sup>
                        <span>39</span>
                    </p>
                    <p class="hint">12 months access to this theme</p>
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
 		    <li><span class="ion-star"></span>1 year of updates</li>
                    <li><span class="ion-earth"></span>1 year of support</li>
                    <li><span class="ion-ios-cloud"></span>Unlimited Domains/Websites</li>
                    <li><span class="ion-android-done"></span>30-day money back guarantee</li>
                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer">
                    <p><a href="https://oceanwebthemes.com/checkout?edd_action=add_to_cart&download_id=<?php echo get_the_ID(); ?>">BUY NOW</a></p>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /PERSONAL -->
            <!-- PROFESSIONAL -->
            <div class="block professional fl">
                <h2 class="title">Developer</h2>
                <!-- CONTENT -->
                <div class="content">
                    <p class="price">
                        <sup>$</sup>
                        <span>89</span>
                    </p>
                    <p class="hint">12 months access to all themes</p>
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
		    <li><span class="ion-star"></span>1 year of updates</li>
                    <li><span class="ion-earth"></span>1 year of support</li>
                    <li><span class="ion-ios-cloud"></span>Unlimited Domains/Websites</li>
                    <li><span class="ion-android-done"></span>30-day money back guarantee</li>
                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer">
                    <p><a href="https://oceanwebthemes.com/checkout?edd_action=add_to_cart&download_id=13274">BUY NOW</a></p>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /PROFESSIONAL -->
<p class="hint"><br>
All themes can be used for lifetime. You can renew at 50% discount for another year to continue getting updates and support after license expires.</p>
</div> <!-- PRIcing TABLE -->
</section>
</div>
</aside>
<?php } ?>

<?php get_footer(); ?>
